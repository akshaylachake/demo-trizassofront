<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPost extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_post';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
}
