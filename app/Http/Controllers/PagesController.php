<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\MembershipPlan;
use App\Post;
use App\User;
use App\CustomerPost;
use App\CustomerMembershipPlan;
use Storage;
use Illuminate\Support\Facades\Cookie;
class PagesController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function membership()
    { 
        $data = MembershipPlan::get()->all();
        
        return view('pages.membership', compact('data'));
    }

    public function videos()
    {
        $data = Post::get()->all();
        $dataArr = [];
        foreach($data as $key => $value){
            $dataArr[$key] = $value;
            $s3ImagePath = "Sample/" . $value->image;
            $dataArr[$key]->image = Storage::disk('s3')->url($s3ImagePath);
        }
        
        return view('pages.video', compact('dataArr'));
    }

    public function watchvideo($id)
    {   
        if(empty(Auth::user())){
            return redirect('login');
        }
        if(!empty(Auth::user())){
            $data = Post::where('customer_post.customer_id', Auth::user()->id)
                ->where('customer_post.post_id', $id)
                ->join('customer_post', 'customer_post.post_id', '=', 'posts.id')
                ->get()->all();
            if(empty($data)){
                //return redirect('addvideo');
                return redirect()->to('addvideo/'.$id);
            }
            if(!empty($data)){
                $dataArr = [];
                foreach($data as $key => $value){
                    $dataArr[$key] = $value;
                    $videoPath = "Sample/".$value->video;
                    $dataArr[$key]->video = Storage::disk('s3')->url($videoPath);

                    $userArr = User::where('users.id', $value->author_id)
                                ->get()->first();
                    $dataArr[$key]->auther = $userArr->name;
                }
                return view('pages.watchvideo', compact('dataArr'));
            }
        } 
    }

    public function addvideo($id)
    {   
        return view('pages.addvideo', compact('id'));
    }

    public function addmembership($id)
    {   
        return view('pages.addmembership');
    }

    public function submitmembership()
    {   
        return redirect('usermembership');
    }

    public function submitvideo()
    {   
        return redirect()->to('uservideo');
    }

    public function gotovideo($id)
    {   
        if(empty(Auth::user())){
            return redirect('login');
        }
        if(!empty(Auth::user())){
            $data = CustomerPost::where('customer_post.customer_id', Auth::user()->id)
                ->where('customer_post.post_id', $id)
                ->get()->all();
            if(empty($data)){
                return redirect()->to('addvideo/'.$id);
            }
        }
        if(!empty($data)){
            return redirect()->to('watchvideo/'.$id);
        }
    }
    public function gotomembership($id)
    {   
        if(empty(Auth::user())){
            return redirect('login');
        }
        if(!empty(Auth::user())){
            $data = CustomerMembershipPlan::where('customer_membership_plan.customer_id', Auth::user()->id)
                ->where('customer_membership_plan.membership_plan_id', $id)
                ->where('customer_membership_plan.is_active', 1)
                ->get()->all();
            if(empty($data)){
                return redirect()->to('addmembership/'.$id);
            }
        }
        if(!empty($data)){
            return redirect('usermembership');
        }
    }

    public function uservideo()
    {   
        $data = Post::where('customer_post.customer_id', Auth::user()->id)
                ->join('customer_post', 'customer_post.post_id', '=', 'posts.id')
                ->get()->all();
        
        return view('pages.uservideo', compact('data'));
    }

    public function usermembership()
    {
        $data = MembershipPlan::where('customer_membership_plan.customer_id', Auth::user()->id)
                ->join('customer_membership_plan', 'customer_membership_plan.membership_plan_id', '=', 'membership_plans.id')
                ->get()->all();
        
        return view('pages.usermembership', compact('data'));
    }

    public function gotologout(Request $request)
    {
        Cookie::queue(Cookie::forget('sessiondata'));
        unset($_SESSION);
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }
}
