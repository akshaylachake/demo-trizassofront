@extends('layouts.app')

@section('content')
<?php 
    $serverIp = config('constants.server');
?>
<div class="dashboard login-Page">
    <div class="container">
        <div class="main">
            <div class="col-md-3">
                <div class="sidebar">
                    <ul>
                        <li class="active"><a href="{{ url('home') }}"><i class="fa fa-tachometer" ></i> Dashboard</a></li>
                        <li><a href="{{ url('uservideo') }}"><i class="fa fa-video-camera" ></i> Videos</a></li>
                        <li><a href="{{ url('usermembership') }}"><i class="fa fa-users" ></i> Membership plans</a></li>
                        <li><a href="#"><i class="fa fa-money" ></i> Payment Pending</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizassowpdemo/"><i class="fa fa-arrow-left" ></i> Go to Website</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="dash_Data">
                    <div class="dashboardTab">
                        <div class="tabhead">
                            <h1>DASHBOARD</h1>
                        </div>
                        <div class="dash_deatils">
                            <div class="col-md-6">
                                <div class="dashUI">
                                    <div class="col-md-7 text-center no-pad">
                                        <h3> {{ $totalVideos }}</h3>
                                        <h4> Total Videos</h4>
                                    </div>
                                    <div class="col-md-5">
                                        <i class="fa fa-video-camera" ></i>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="dashUI">
                                    <div class="col-md-7 text-center no-pad">
                                        <h3> {{ $totalMemberships }} </h3>
                                        <h4>Total Membership plan</h4>
                                    </div>
                                    <div class="col-md-5">
                                        <i class="fa fa-users" ></i>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="dashUI" style="padding: 15px;">
                                    <h2> Active Membership plan </h2> 
                                    <ul>
                                        @php  $sn = 1;@endphp
                                        @foreach($membership as $item) 
                                            <li>Title : {{ $item->title }}</li>
                                            <li>Description : {{ $item->description }}</li>
                                            <li>Price : {{ $item->cost }}</li>
                                            <li>Validity : {{ $item->validity }}</li>
                                        @php  $sn++;@endphp   
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection
