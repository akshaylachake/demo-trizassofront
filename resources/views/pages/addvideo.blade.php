@extends('layouts.app')

@section('content')
<?php 
    $serverIp = config('constants.server');
?>
<div class="dashboard login-Page">
    <div class="container">
        <div class="main">
            <div class="col-md-3">
                <div class="sidebar">
                    <ul>
						<li><a href="{{ url('home') }}"><i class="fa fa-tachometer" ></i> Dashboard</a></li>
                        <li><a href="{{ url('uservideo') }}"><i class="fa fa-video-camera" ></i> Videos</a></li>
                        <li><a href="{{ url('usermembership') }}"><i class="fa fa-users" ></i> Membership plans</a></li>
                        <li><a href="#"><i class="fa fa-money" ></i> Payment Pending</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/"><i class="fa fa-arrow-left" ></i> Go to Website</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="dash_Data">
                    <div class="dashboardTab">
                    <form method="POST" action="{{ url('submitvideo') }}">
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $id }}">
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control" name="input" required placeholder="Text">
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                        <div class="text-center">
                             <button type="submit" class="btn-primary m-r-20">Submit</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection


