@extends('layouts.app')

@section('content')
<div class="videoPage login-Page commoncss membership">
    <div class="bannerImage parallax">
        <h1>Membership</h1>
    </div>
    
    <div class="memberSection">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="../images/img-experience.png">
                    <a href="http://trizassociation.org/?page_id=3009" title="Know More About Our Membership"><button class="btn-primary">Know More About Our Membership</button></a>
                </div>
                <div class="col-md-6">
                    <div class="memberContent">
                        <div class="sec sec1">
                            <h2>Student <span> Member</span></h2>
                            <p>Student member shall be a person enrolled by the association</p>
                            <ul>
                                <li>is between 10-21 Years of age</li>
                                <li>is registered as a student of any stream.</li>
                            </ul>
                            <p>After the age of 21 years he will be eligible to become regular Member on fulfilment of requirements of the particular membership of the association.</p>
                        </div>
                        <div class="sec sec2">
                            <h2>Regular <span>Member</span></h2>
                            <p>Any Indian person, enrolled by the association on application, from any engineering or non engineering field who wish to be associated with TRIZ. He may or may not have TRIZ qualification or he may or may not be directly involved in TRIZ.</p>
                            <p>Such person should be at least 21 years of age. There will be no upper age limit for this membership. This membership is currently free of cost.</p>
                        </div>
                        <div class="sec sec3">
                            <h2><span>Membership</span> allows you to enjoy features like</h2>
                            <ul>
                                <li>Receiving news and information about the recent development in the TRIZ community</li>
                                <li>Availing discounted rates to attend events & sessions organized by TAA, PAN India</li>
                            </ul>
                        </div>
                        <div class="sec sec4">
                            <h2><span>Membership</span> Fee</h2>
                            <p>The membership to TRIZ Association of Asia is free, It can be obtained by registering as a user at this website by following this link</p>
                            <p>There would be no charges applicable as of now. The association has all the right going further to apply charges for becoming a member. The existing members have an option then to continue or move out of the membership then.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>








    <div class="container">
        <h2> Membership <span>Plans</span></h2>
        @php  $sn = 1;@endphp
        @foreach($data as $item) 
        <div class="col-md-4">
            <div class="videoBox">
                <div class="package-header">
                    <div class="border"></div> 
                    <h1><span>{{ $item->title }}</span></h1>
                </div>
                <div class="col-md-6 no-pad text-center">
                    <div class="borderRight">
                        <p>COST<span>{{ $item->cost }}</span></p>
                    </div>
                </div>
                <div class="col-md-6 no-pad text-center">
                    <p>VALIDITY<span>{{ $item->validity }}</span> </p>
                </div>
                <div class="clearfix"></div>

                <ul class="membershpipFeature">
                    <li>{{ $item->description }}</li>
                </ul>
                <div class="text-center">
                    
                    <a href="{{ url('addmembership/'.$item->id) }}"><button class="btn-primary">Buy Now</button></a>

                </div>
            </div>
        </div>
        @php  $sn++;@endphp   
        @endforeach
    </div>
</div>
@endsection
