@extends('layouts.app')

@section('content')
<div class="videoPage login-Page newcss">
    <div class="container">
    <div class="clearfix"></div>
    @php  $sn = 1;@endphp
        @foreach($dataArr as $item) 

        <div class="col-md-4">
            <div class="videoBox">
                <div class="imgBox">
                    <img src="{{ $item->image }}" />
                </div>
                <h1>{{ $item->title }}</h1>
                <p>COST : <span>{{ $item->price }}</span></p>
                <div class="text-center">
                    <a href="{{ url('watchvideo/'.$item->id) }}"><button class="btn-primary">Watch Now</button></a>
                </div>
            </div>
        </div>
        @php  $sn++;@endphp
        <?php if($sn % 3 == 1){ ?>
         <div class="clearfix"></div>
        <?php } ?> 
        @endforeach
    </div>
</div>
@endsection
