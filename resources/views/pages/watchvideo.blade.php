@extends('layouts.app')

@section('content')
<div class="videoPage login-Page">
    <div class="container">
        <div class="video-details">
		@php  $sn = 1;@endphp
        @foreach($dataArr as $item) 
        	<div class="videoTag">
        		 <video controls>
					 <source src="{{ $item->video }}" type="video/mp4">
				</video> 
			</div>
			<h1>{{ $item->slug }}</h1>
			<!-- <h3 class="subHead">Become an expert at SQL! </h3> -->
			<h2>Created by <a href=""> {{ $item->auther }}</a></h2>
			<p><i class="fa fa-exclamation-circle" aria-hidden="true"></i> {{ $item->updated_at }} </p>
    		<div>
    			<h1>About Video</h1>
    			{{ $item->body }}
    		</div>
			@php  $sn++;@endphp   
        @endforeach
        </div>
    </div>
</div>
@endsection
