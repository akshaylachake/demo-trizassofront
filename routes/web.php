<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('gotologout', 'PagesController@gotologout');
Route::get('videos', 'PagesController@videos');
Route::get('membership', 'PagesController@membership');
Route::get('/', function () {
    return redirect('http://'.config('constants.server').'/trizassowpdemo/');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/home', 'HomeController');
    Route::get('usermembership', 'PagesController@usermembership');
    Route::get('uservideo', 'PagesController@uservideo');
    Route::get('watchvideo/{id}', 'PagesController@watchvideo');
    Route::get('addvideo/{id}', 'PagesController@addvideo');
    Route::get('addmembership/{id}', 'PagesController@addmembership');
    Route::post('submitmembership', 'PagesController@submitmembership');
    Route::post('submitvideo', 'PagesController@submitvideo');
    Route::get('gotomembership/{id}', 'PagesController@gotomembership');
    Route::get('gotovideo/{id}', 'PagesController@gotovideo');
  });